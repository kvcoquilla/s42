// DOM - document object model

const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName =  document.querySelector('#txt-last-name')
const spanFullName =  document.querySelector('#span-full-name')

txtFirstName.addEventListener('keyup', (event) => {
	fullname(txtFirstName.value);
})

txtLastName.addEventListener('keyup', (event) => {
	fullname(txtLastName.value);
})

function fullname(name){
	spanFullName.innerHTML = txtFirstName.value + ' ' +txtLastName.value;
}